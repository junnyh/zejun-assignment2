#' Update blue cars' position funtion
#' 
#' This function allows you to move blue cars 1 step upward
#' @param blues, reds, occupied
#' @return blue_position, occupied grid
#' @keywords update_position_blue
#' @export

update_position_blue = function(blues, reds, occupied) { 
  num_blue_cars = nrow(blues)
  x = blues[,1]
  y = blues[,2]
  r = nrow(occupied)
  c = ncol(occupied)
  # start moving toward upward
  
  next_blues_x = ifelse(x==1, r, x-1) # next x potentially
  next_blues = cbind(next_blues_x, y) # next blues potentially
  will_occupied = occupied[next_blues]
  x = ifelse(!will_occupied, next_blues_x, x) # if occupied, then keep the current position
  
  occupied = matrix(FALSE, nrow = r, ncol = c)
  new_blues = cbind(x, y)
  occupied[new_blues] = TRUE
  occupied[reds] = TRUE
  return(list(new_blues, occupied))
}